/* 
USING DOM
before, we are using the following codes in terms of selecting elements inside the html file
*/

// document refers to the whole webpage and the getElementById selects the element with the same id as its arguments ("")
document.getElementById('txt-first-name');

document.getElementsByClassName('txt-last-name');
document.getElementsByTagName('input');

// querySelector replaces the three "getElement" selectors and makes use of css format in terms of selecting the elements inside tht html as its arguments (# - id, . - class, tagName - tag)
document.querySelector('#txt-first-name');

// Even Listeners - events are all interactions of the user to our webpagel such as clicking, hovering, reloading,keypress/keyup(typing), scroll

const txtFirstName = document.querySelector('#txt-first-name');
const spanFullName = document.querySelector('#span-full-name');


// to perform an action when an event triggers, first you have to listen it
// keyup means typing (one at a time)
// keypress holding down of a key


/* 
the function used is the addEventListener - allows a block of codes to listen to an event for them to be executed
    addEventListener - takes two arguments: 
    1 - a string that identifies the event 
    2- a function that the listener will execute once the specified event is triggered

*/
txtFirstName.addEventListener('keyup',(event) => {
    // innerHTML - this allws the element to record/duplicate the value of the selected variable (txtFirstName.value)
    // .value to indicated that you innerHTML should record the actual value not the type of element
    spanFullName.innerHTML = txtFirstName.value;
});

// multiple listeners can also be assigned to same event, in the same way, same listeners can listen to different events

// 
txtFirstName.addEventListener('keyup',(event) => {
    // event - contains the information the triggered event passed from the first argument
    // event.target- contains the element where the event happend
    // event.target.value - gets the value of the input object where the event happened
    console.log(event.target);
    console.log(event.target.value);
})
